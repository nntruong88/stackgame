﻿Shader "StackShader" {
	Properties{
		_MainTex ("Base (RGB)", 2D) = "while"{}
	}
	SubShader{
		Pass
		{
			Lighting On
			ColorMaterial AmbientAndDiffuse
			SetTexture [_MainTex]
			{
				combine texture * primary DOUBLE
			}
		}
	}
}
