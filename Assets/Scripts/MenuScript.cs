﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {

	[SerializeField]
	private Text txtScore;

	// Use this for initialization
	private void Start () 
	{
		int score = PlayerPrefs.GetInt ("SCORE", 0);
		txtScore.text = score.ToString ();
	}

	public void OnPlay()
	{
		SceneManager.LoadScene ("Game");
	}	
}
