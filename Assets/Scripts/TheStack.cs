﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TheStack : MonoBehaviour {
	private const float BOUND_SIZE = 3.5f;
	private const float STACK_MOVING_SPEED = 5.0f;
	private const float ERROR_MARGIN = 0.25f;

	private GameObject[] theStacks;

	private int stackIndex;
	private int scoreCount;
	private int colorIndex;

	private Vector3 lastTilePosition;
	private Vector3 targetStack;
	private Vector2 stackBounds = new Vector2(BOUND_SIZE, BOUND_SIZE);

	private bool isMoveX;
	private bool isGameover;

	private float tileSpeed = 2.5f;
	private float tileTransaction;
	private float secondaryPosition;
	private float transactionColor;

	public Color32[] gameColors;
	public Material stackMat;
	private Color32 colorStart;
	private Color32 colorEnd;

	[SerializeField]
	private Text txtScore;

	[SerializeField]
	private GameObject panel;

	[SerializeField]
	private GameObject gObjPrefab;

	[SerializeField]
	private GameObject rootObjMove;

	// Use this for initialization
	private void Start () 
	{
		isMoveX = true;
		scoreCount = 0;
		panel.SetActive (false);
		txtScore.text = "0";
		theStacks = new GameObject[transform.childCount];
		colorStart = gameColors [0];
		colorEnd = gameColors [1];
		colorIndex = 1;
		GameObject gObj;
		for (int i = 0, len = transform.childCount; i < len; i++) {
			gObj = transform.GetChild (i).gameObject;
			CreateColor (gObj.GetComponent<MeshFilter>().mesh);
			theStacks [i] = gObj;
		}
		stackIndex = transform.childCount - 1;

	}
	
	// Update is called once per frame
	private void Update () 
	{
		if(isGameover)
			return;
		
		if (Input.GetMouseButtonDown (0)) 
		{
			if (PlaceTile ()) 
			{
				SpawnTile ();
				scoreCount++;
				txtScore.text = scoreCount.ToString ();
			}
		}

		MoveTile ();

		// update position of Stack
		this.transform.position = Vector3.Lerp(this.transform.position, targetStack, Time.deltaTime * STACK_MOVING_SPEED);
	}

	private bool PlaceTile()
	{
		Transform t = theStacks [stackIndex].transform;
		float delta, middle;
		if (isMoveX) {
			delta = lastTilePosition.x - t.position.x;

			if (Mathf.Abs (delta) > ERROR_MARGIN) 
			{
				//replace by x
				stackBounds.x -= Mathf.Abs(delta);
				if (stackBounds.x <= 0) {
					EndGame ();
					return false;
				}
				t.transform.localScale = new Vector3 (stackBounds.x, 1, stackBounds.y);
				middle = lastTilePosition.x + t.transform.localPosition.x / 2;
				t.transform.localPosition = new Vector3 (middle - (lastTilePosition.x / 2), scoreCount, lastTilePosition.z);

				CreateRemovePart (
					new Vector3(
						(t.position.x > 0) ? t.position.x + (t.localScale.x/2) : t.position.x - (t.localScale.x/2),
						t.position.y,
						t.position.z
					),
					new Vector3(Mathf.Abs(delta), 1, t.localScale.z)
				);
			}
			else 
			{
				t.transform.localPosition = new Vector3 (lastTilePosition.x, scoreCount, lastTilePosition.z);
			}
		} 
		else 
		{
			delta = lastTilePosition.z - t.position.z;
			if (Mathf.Abs (delta) > ERROR_MARGIN) 
			{
				//replace by z
				stackBounds.y -= Mathf.Abs(delta);
				if (stackBounds.y <= 0) 
				{
					EndGame ();
					return false;
				}
				t.transform.localScale = new Vector3 (stackBounds.x, 1, stackBounds.y);
				middle = lastTilePosition.z + t.transform.localPosition.z / 2;
				t.transform.localPosition = new Vector3 (lastTilePosition.x, scoreCount, middle - (lastTilePosition.z / 2));

				CreateRemovePart (
					new Vector3(
						t.position.x,
						t.position.y,
						(t.position.z > 0) ? t.position.z + (t.localScale.z/2) : t.position.z - (t.localScale.z/2)
					),
					new Vector3(t.localScale.x, 1, Mathf.Abs(delta))
				);
			}
			else 
			{
				t.transform.localPosition = new Vector3 (lastTilePosition.x, scoreCount, lastTilePosition.z);
			}
		}
		secondaryPosition = (isMoveX) ? t.transform.localPosition.x : t.transform.localPosition.z;
		isMoveX = !isMoveX;
		return true;
	}

	private void SpawnTile()
	{
		tileTransaction = 0;
		lastTilePosition = theStacks [stackIndex].transform.localPosition;
		stackIndex--;
		if (stackIndex < 0)
			stackIndex = transform.childCount - 1;
		targetStack = (Vector3.down) * scoreCount;
		Transform transIndex = theStacks [stackIndex].transform;
		transIndex.localScale = new Vector3 (stackBounds.x, 1, stackBounds.y);
		transIndex.localPosition = new Vector3 (0, scoreCount, 0);
		CreateColor (transIndex.transform.GetComponent<MeshFilter>().mesh);
	}

	private void MoveTile()
	{
		tileTransaction += Time.deltaTime * tileSpeed;
		if(isMoveX)
			theStacks [stackIndex].transform.localPosition = new Vector3 (Mathf.Sin (tileTransaction) * BOUND_SIZE, scoreCount, secondaryPosition);
		else
			theStacks [stackIndex].transform.localPosition = new Vector3 (secondaryPosition, scoreCount, Mathf.Sin (tileTransaction) * BOUND_SIZE);
	}

	void CreateRemovePart(Vector3 pos, Vector3 scale)
	{
		GameObject gObj = Instantiate (gObjPrefab, Vector3.zero, Quaternion.identity, rootObjMove.transform);
		gObj.transform.localPosition = pos;
		gObj.transform.localScale = scale;
		gObj.SetActive (true);
		//gObj.AddComponent<Rigidbody> ();
		gObj.GetComponent<Renderer>().material = stackMat;
		CreateColor (gObj.GetComponent<MeshFilter>().mesh);
	}

	private void EndGame()
	{
		isGameover = true;
		panel.SetActive (true);
		PlayerPrefs.SetInt ("SCORE", scoreCount);
	}

	public void OnRetry ()
	{
		SceneManager.LoadScene ("Game");
	}

	public void OnMenu()
	{
		SceneManager.LoadScene ("Menu");
	}

	private void CreateColor(Mesh mesh)
	{
		transactionColor += 0.1f;
		Vector3[] ver = mesh.vertices;
		Color32[] colors = new Color32[ver.Length];

		if (transactionColor > 1) {
			transactionColor = 0;
			colorStart = colorEnd;
			int ci = colorIndex;
			while (ci == colorIndex) {
				ci = Random.Range(0, gameColors.Length);
			}
			colorIndex = ci;
			colorEnd = gameColors [ci];
		}
		Color c = Color.Lerp (colorStart, colorEnd, transactionColor);
		for (int i = 0, len = colors.Length; i < len; i++) 
		{
			colors [i] = c;
		}
		mesh.colors32 = colors;
	}
}
